import Home from "./Home";
import Renarrate from "./Renarrate";
import Dashboard from "./Dashboard";
import LocateXPath from "./LocateXPath";
import Render from "./Render";
import RenderJSON from "./RenderJSON";
import Landing from "./Landing";
import Footer from "../Pages/Footer";


export { Home, Renarrate, Dashboard, LocateXPath, RenderJSON, Render, Landing, Footer };