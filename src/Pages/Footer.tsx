import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-900 py-4">
      <div className="container mx-auto flex flex-col sm:flex-row justify-between px-4">
        <div className="text-gray-500 text-left order-last sm:order-first sm:w-1/2">
          <span>© 2023 </span>
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://open.janastu.org"
          >
            janastu.org
          </a>
          <span>, Inc. </span>
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://gitlab.com/servelots/papad/papad-api/-/blob/prod/LICENSE"
          >
            GNU AGPLv3
          </a>
        </div>
        <div className="flex flex-col text-gray-500 text-right order-first sm:order-last sm:w-1/2">
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://gitlab.com/servelots/renarration"
          >
            Renarration Repo
          </a>
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://gitlab.com/groups/servelots/renarration/-/issues"
          >
            File an issue
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
