import axios from "axios";
import React, {  useState, useRef } from "react";
import {  Space, Input, Button } from "antd";
import { useNavigate } from "react-router-dom";
import type { InputRef } from "antd";

const Landing = () => {
  const [url, setUrl] = useState("");
  const [error, setError] = useState(null);
  const inputRef = useRef<InputRef>(null);
  const readURL = localStorage.getItem("read_url");
  const [loading, setLoading] = useState<boolean>(false);

  const navigate = useNavigate();

  const handleURL = async () => {
    const options = {
      method: "POST",
      url: `${readURL}/api/parse`,
      headers: { "Content-Type": "application/json" },
      data: { url: url },
    };

    axios
      .request(options)
      .then(function (response) {
        localStorage.setItem("readability", JSON.stringify(response.data));
        navigate(`/renarrate?url=${url}`);
        setLoading(false);
      })
      .catch(function (error) {
        console.error(error);
        setError(error.response.data);
        setLoading(false);
      });
  };

  const onURLChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value);
  };

  return (
    <div className="p-5 space-x-5 max-w-7xl py-8 mx-auto">
      <div className="flex items-center justify-center">
        <form
          onSubmit={() => {
            handleURL();
          }}
          className="flex w-full"
        >
          <Space direction="vertical" style={{ width: "100%" }}>
            <Space wrap>
              <Input
                placeholder="Parse article"
                ref={inputRef}
                value={url}
                onChange={onURLChange}
              />
              <Button
                onClick={() => {
                  handleURL();
                  setLoading(true);
                }}
                type="primary"
                loading={loading}
              >
                Parse Article
              </Button>
            </Space>
          </Space>
        </form>
      </div>

      {error && <div className="">{error}</div>}
    </div>
  );
};

export default Landing;
