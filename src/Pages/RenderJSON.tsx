import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Space } from "antd";
import { LinkOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { RenderContent } from "../Components";

const RenderJSON = () => {
  const [data, setData] = useState<any>();
  const [url, setUrl] = useState<string>("");
  const [article, setArticle] = useState<any>(null);
  const [error, setError] = useState(null);
  const [params, setParams] = useState<any>("");
  const readURL = localStorage.getItem("read_url");

  const navigate = useNavigate();

  useEffect(() => {
    const param = new URLSearchParams(window.location.search);
    setParams(param);

    const jsonData = param.get("data");

    if (jsonData !== null) {
      const data = JSON.parse(jsonData);
      setData(data);
    }
  }, []);

  useEffect(() => {
    if (data?.url !== undefined && data?.url !== "") {
      setUrl(data?.url);
      localStorage.setItem("sweet", JSON.stringify([data]));
    }
  }, [data]);

  useEffect(() => {
    if (url !== undefined && url !== "") {
      parseJSONArticle();
    }
  }, [url]);

  useEffect(() => {
    if (article !== undefined && article !== "") {
      if (data?.xpath !== "" && data?.rennarated_text !== "") {
        handleReno(data?.xpath, data?.rennarated_text);
      }
    }
  }, [article]);

  const parseJSONArticle = async () => {
    const options = {
      method: "POST",
      url: `${readURL}/api/parse`,
      headers: { "Content-Type": "application/json" },
      data: { url: url },
    };

    axios
      .request(options)
      .then(function (response) {
        localStorage.setItem("readability", JSON.stringify(response.data));
        setArticle(response.data);
      })
      .catch(function (error) {
        console.error(error);
        setError(error.response.data);
      });
  };

  const handleReno = async (renXPath?: any, renTitle?: any) => {
    const xpath = renXPath;

    const result = document.evaluate(
      xpath,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    );
    const element = result.singleNodeValue;

    if (element instanceof HTMLElement) {
      console.log("Element found:", element);
      element.style.backgroundColor = "yellow";
      element.innerHTML = renTitle;

      // const titleElement = document.createElement("div");
      // titleElement.innerHTML = renTitle;

      const hoverEvent = new MouseEvent("mouseover", {
        bubbles: true,
        cancelable: true,
        view: window,
      });
      element.dispatchEvent(hoverEvent);
    } else {
      console.log("Element not found");
    }
  };

  return (
    <>
      <div className="h-screen flex justify-center items-center">
        <Space direction="vertical" size="middle" style={{ display: "flex" }}>
          <Card
            title="Message"
            size="small"
            // actions={[
            //   <LinkOutlined
            //     key="link"
            //     onClick={() => navigate(`/ren?${params}`)}
            //     // onClick={() => navigate(`/`)}
            //   />,
            // ]}
          >
            <p>URL : {data?.url}</p>
            <p>XPath : {data?.xpath}</p>
            <p>Renarrated Text : {data?.rennarated_text}</p>
          </Card>
        </Space>
      </div>
      <div>
        <RenderContent title={article?.title} content={article?.content} />
      </div>
      {error && <div className="">{error}</div>}
    </>
  );
};

export default RenderJSON;
