import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { ModalComponent, TextEditorComponent } from "../Components";
import { Card, Input, message, Space, Typography } from "antd";
import { useNavigate } from "react-router-dom";
import { v4 } from "uuid";
import { Button, notification } from "antd";
import copy from "clipboard-copy";
import { LinkOutlined } from "@ant-design/icons";

type NotificationType = "success" | "info" | "warning" | "error";
const { TextArea } = Input;
const { Paragraph } = Typography;

const Home = ({
  textData,
  title,
  renUrl,
  updateRenarration,
  addRen,
  addRenTitle,
}: {
  textData?: string;
  title?: string;
  renUrl: string;
  updateRenarration: Function;
  addRen: boolean;
  addRenTitle: string;
}) => {
  const [renoModal, setRenoModal] = useState(false);
  const [selectedText, setSelectedText] = useState<string>("");
  const [selectedImage, setSelectedImage] = useState<any>();
  const [rennaratedText, setRennaratedText] = useState<string>("");
  const [loading, setLoading] = useState(false);
  const selectedRef = useRef<HTMLDivElement | null>(null);
  const XPATHRef = useRef<HTMLDivElement>(null);
  const [renXPath, setRenXPath] = useState<string>("");
  const [renTitleModal, setRenTitleModal] = useState(false);
  const [renarratedTitle, setRenarratedTitle] = useState<string>("");
  const renLocal = localStorage.getItem("renarration");
  const [renData, setRenData] = useState<any>([]);
  const [api, contextHolder] = notification.useNotification();
  const [shareModal, setShareModal] = useState(false);
  const textRef = useRef<HTMLDivElement>(null);
  const baseURL = localStorage.getItem("prod_url");
  const sweet = localStorage.getItem("sweet");
  const targetRef = useRef<HTMLDivElement>(null);

  const navigate = useNavigate();

  const openNotificationWithIcon = (
    type: NotificationType,
    message,
    description?
  ) => {
    api[type]({
      message: message,
      description: "",
    });
  };

  const handleCopy = () => {
    if (textRef.current) {
      copy(textRef.current.innerText);
      message.success("Copied to clipboard");
    }
  };

  useEffect(() => {
    if (selectedText !== "") {
      setRenoModal(true);
    }
  }, [selectedText]);

  useEffect(() => {
    if (addRen) {
      addRenTitle !== "" && postRenoTitle();
    }
  }, [addRen]);

  useEffect(() => {
    if (sweet !== undefined && sweet !== null && sweet !== "") {
      setRenData(JSON.parse(sweet));
    }
  }, []);

  useEffect(() => {
    if (renData !== undefined && renData !== null) {
      renData.forEach((value) => {
        handleSubmit(value.xpath, value.rennarated_text);
      });
    }
  }, [renData]);

  const renderNode = (ren, type) => {
    const node = document.evaluate(
      ren.target.value,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    ).singleNodeValue;
    if (node && type === "kn") {
      if (ren.motivation === "kan-text") {
        node.textContent = ren.body.value;
      } else if (ren.motivation === "kan-img") {
        (node as HTMLImageElement).src = ren.body.value;
      }
    } else if (node && type === "hi") {
      if (ren.motivation === "hin-text") {
        node.textContent = ren.body.value;
      } else if (ren.motivation === "hin-img") {
        (node as HTMLImageElement).src = ren.body.value;
      }
    }
  };

  const postReno = () => {
    setLoading(true);

    setRenData((prevData) => [
      ...prevData,
      {
        ["url"]: renUrl,
        ["xpath"]: renXPath,
        ["text"]: selectedText,
        ["rennarated_text"]: rennaratedText,
      },
    ]);

    setLoading(false);
    setRenoModal(false);
    handleSubmit(renXPath, rennaratedText);
    openNotificationWithIcon("success", "Sweet is successful");
  };

  const HandleSelectedClick = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    selectedRef.current = event.target as HTMLDivElement;
    if (selectedRef.current) {
      const selectedElementText = selectedRef.current.innerText;
      setRenoModal(true);

      if (selectedElementText === "") {
        setSelectedImage(selectedRef.current);
        setSelectedText("");
        setRennaratedText("");
      } else {
        setSelectedText(selectedElementText);
        setRennaratedText(selectedElementText);
      }
    }
  };

  // Relative XPath
  const getXPathForElement = (el: Element, xml: Document): string => {
    let xpath = "";
    let pos: number, tempitem2: Node | null;

    while (el !== null && el.nodeType === 1) {
      pos = 0;
      tempitem2 = el.previousSibling;

      while (tempitem2) {
        if (tempitem2.nodeType === 1 && tempitem2.nodeName === el.nodeName) {
          pos += 1;
        }
        tempitem2 = tempitem2.previousSibling;
      }

      const idAttr = el.getAttribute("id");
      const classAttr = el.getAttribute("class");

      if (idAttr) {
        xpath = `//${el.nodeName.toLowerCase()}[@id='${idAttr}']${
          xpath !== "" ? "/" : ""
        }${xpath}`;
        break;
      } else if (classAttr) {
        const classes = classAttr;
        const classSelectors = `${el.nodeName.toLowerCase()}[@class='${classes}']`;
        xpath = `//${classSelectors}${xpath !== "" ? "/" : ""}${xpath}`;
        break;
      } else {
        xpath = `/${el.nodeName.toLowerCase()}[${pos + 1}]${
          xpath !== "" ? "/" : ""
        }${xpath}`;
      }

      el = el.parentNode as Element;
    }

    return xpath;
  };

  const handleXPATH = (event) => {
    const element = event.target;
    const xmlDocument = document;
    const xpath = getXPathForElement(element, xmlDocument);
    setRenXPath(xpath);
  };

  const postRenoTitle = () => {
    const uuid = v4();

    const options = {
      method: "POST",
      url: `${baseURL}/renarration`,
      headers: { "Content-Type": "application/json" },
      data: {
        uuid: uuid,
        title: addRenTitle,
        url: renUrl,
        renstore: renData,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        setRenData([]);
        setRenarratedTitle("");
        setRenTitleModal(false);
        openNotificationWithIcon("success", "Renarration is successful");
        localStorage.setItem("sweet", "");
        navigate(`/renarrate/${response.data?.uuid}`);
      })
      .catch((error) => {
        console.error(error);
        openNotificationWithIcon("success", "Failed to renarrate");
      });
  };

  console.log(renData);

  const handleSubmit = async (renXPath?: any, renTitle?: any) => {
    const xpath = renXPath;

    const result = document.evaluate(
      xpath,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    );
    const element = result.singleNodeValue;

    if (element instanceof HTMLElement) {
      console.log("Element found:", element);

      element.style.backgroundColor = "yellow";
      element.innerHTML = renTitle;

      element.scrollIntoView({ behavior: "smooth" });

      // const spaceElement = document.createElement("span");
      // spaceElement.style.marginRight = "0";
      // spaceElement.style.position = "absolute";
      // spaceElement.style.right = "0";
      // element.appendChild(spaceElement);

      // const titleElement = document.createElement("span");

      // titleElement.innerHTML = renTitle;
      // spaceElement.appendChild(titleElement);

      // const toggleButton = document.createElement("button");
      // toggleButton.innerHTML = "Toggle";
      // toggleButton.addEventListener("click", () => {
      //   // Toggle the visibility of the element
      //   element.style.display =
      //     element.style.display === "none" ? "block" : "none";
      // });
      // spaceElement.appendChild(toggleButton);

      const hoverEvent = new MouseEvent("mouseover", {
        bubbles: true,
        cancelable: true,
        view: window,
      });
      element.dispatchEvent(hoverEvent);
    } else {
      console.log("Element not found");
    }
  };

  const [data, setData] = useState<any>(null);

  useEffect(() => {
    const param = new URLSearchParams(window.location.search);

    const jsonData = param.get("data");

    if (jsonData !== null) {
      const data = JSON.parse(jsonData);
      setData(data);
    }
  }, []);

  useEffect(() => {
    handleSubmit(data?.xpath, data?.rennarated_text);
  }, [data]);

  const addASweet = () => {
    if (data?.url !== undefined && data?.url !== "") {
      // setUrl(data?.url);
      localStorage.setItem("sweet", JSON.stringify([data]));
    }
  };

  const RenMessage = () => {
    return (
      <Space direction="vertical" size="middle" style={{ display: "flex" }}>
        <Card
          title="Message"
          extra={
            <span>
              Scroll down to see the renarration highlighted in yellow
            </span>
          }
          size="small"
          actions={[
            <div
              className="space-x-5"
              onClick={() => {
                addASweet();
                navigate(`/renarrate?url=${renUrl}`);
              }}
            >
              <span>Add a Label</span>
              <LinkOutlined key="link" />
            </div>,
          ]}
        >
          <p>URL : {data?.url}</p>
          <p>XPath : {data?.xpath}</p>
          <p>Renarrated Text : {data?.rennarated_text}</p>
        </Card>
      </Space>
    );
  };

  console.log(renUrl);

  return (
    <>
      {contextHolder}
      <div className="flex items-center justify-center gap-5">
        <Typography>
          {data !== null && <RenMessage />}

          {textData && title && (
            <div
              onClick={(e) => {
                HandleSelectedClick(e);
                handleXPATH(e);
              }}
              className="p-2 mx-5 my-5 border rounded-lg  max-w-3xl shadow hover:shadow-lg hover:border-blue-500 transition duration-150 ease-in-out"
              ref={XPATHRef}
            >
              <h1 className="text-3xl font-bold mb-4 flex justify-center">
                {title}
              </h1>

              <div
                className="prose text-gray-700 mb-2"
                dangerouslySetInnerHTML={{ __html: textData }}
              />
            </div>
          )}
        </Typography>
      </div>
      {renoModal && (
        <ModalComponent
          open={renoModal}
          onClose={() => setRenoModal(false)}
          title={"Add a Sweet"}
          onSuccess={() => {
            postReno();
          }}
          loading={loading}
          width="90%"
          footer={[
            <Button key="back" onClick={() => setRenoModal(false)}>
              Cancel
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={() => setShareModal(true)}
            >
              Share
            </Button>,
            <Button
              key="link"
              type="primary"
              loading={loading}
              onClick={postReno}
            >
              Continue
            </Button>,
          ]}
        >
          <div className="grid grid-cols-2 gap-5">
            {selectedText === "" ? (
              <div dangerouslySetInnerHTML={{ __html: selectedImage }} />
            ) : (
              <TextArea
                value={selectedText}
                autoSize={{ minRows: 6, maxRows: 11 }}
                readOnly
              />
            )}
            <div>
              <TextEditorComponent
                onChange={(e) => {
                  setRennaratedText(e);
                }}
                value={rennaratedText}
              />
            </div>
          </div>
        </ModalComponent>
      )}
      {renTitleModal && (
        <ModalComponent
          open={renTitleModal}
          onClose={() => setRenTitleModal(false)}
          title={"Add a label to this renarration"}
          onSuccess={() => postRenoTitle()}
          loading={loading}
          width="40%"
        >
          <Input
            onChange={(e) => {
              setRenarratedTitle(e.target.value);
            }}
            value={renarratedTitle}
          />
        </ModalComponent>
      )}
      {shareModal && (
        <ModalComponent
          open={shareModal}
          onClose={() => setShareModal(false)}
          title={"Share sweet"}
          onSuccess={() => postRenoTitle()}
          loading={loading}
          width="40%"
          footer={[
            <Button key="back" onClick={() => setShareModal(false)}>
              Cancel
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={() => {
                handleCopy();
                setShareModal(false);
              }}
            >
              Copy
            </Button>,
          ]}
        >
          <Paragraph ref={textRef}>
            {`${window.location.origin}/ren?data={"rennarated_text":"${rennaratedText}", "url":"${renUrl}", "xpath": "${renXPath}"}`}
          </Paragraph>
        </ModalComponent>
      )}
    </>
  );
};

export default Home;
