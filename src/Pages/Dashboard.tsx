import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { RenderContent } from "../Components";

const Dashboard = () => {
  const [ren, setRen] = useState<any>();
  const [read, setRead] = useState<any>();
  const [error, setError] = useState<any>(null);
  const [renJSON, setRenJSON] = useState<any>([]);

  const { id } = useParams();
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    getRenarration();
  }, []);

  useEffect(() => {
    const readability = localStorage.getItem("readability");

    if (readability !== null) {
      const renstore = renJSON.filter((ren) => ren.uuid === id);
      renstore?.map((store) => setRen(store.renstore));
      setRead(JSON.parse(readability));
    }
  }, [renJSON]);

  useEffect(() => {
    ren?.map((ren) => {
      handleSubmit(ren?.xpath, ren?.rennarated_text);
    });
  }, [ren]);

  const getRenarration = () => {
    const options = { method: "GET", url: `${baseURL}/renarration` };

    axios
      .request(options)
      .then((response) => {
        setRenJSON(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleSubmit = async (renXPath?: any, renTitle?: any) => {
    const xpath = renXPath;

    const result = document.evaluate(
      xpath,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    );
    const element = result.singleNodeValue;

    if (element instanceof HTMLElement) {
      console.log("Element found:", element);
      //   alert(`Element found: ${element}`);
      setError("");
      element.style.backgroundColor = "yellow";
      element.innerHTML = renTitle;

      const hoverEvent = new MouseEvent("mouseover", {
        bubbles: true,
        cancelable: true,
        view: window,
      });
      element.dispatchEvent(hoverEvent);
    } else {
      // Element not found
      console.log("Element not found");
      setError("Element not found");
    }
  };

  return (
    <>
      {ren !== "" && (
        <RenderContent content={read?.content} title={read?.title} />
      )}
      {error && <div className="">{error}</div>}
    </>
  );
};

export default Dashboard;
