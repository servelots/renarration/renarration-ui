import axios from "axios";
import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Renarrate, Dashboard, Render, Landing, Footer } from "./Pages";

const App = () => {
  useEffect(() => {
    baseURL();
  }, []);

  const baseURL = () => {
    const options = {
      method: "GET",
      url: process.env.PUBLIC_URL + `/config.json`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    axios
      .request(options)
      .then((response) => {
        localStorage.setItem("prod_url", response.data.API_URL);
        localStorage.setItem("read_url", response.data.READABILITY_URL);
        console.log(response.data.API_URL);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <div className="bg-indigo-100 h-full min-h-screen max-h-full">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Landing />} />
            <Route path="/renarrate" element={<Renarrate />} />

            <Route path="/renarrate/:id" element={<Dashboard />} />
            <Route path="/ren" element={<Renarrate />} />

            {/* <Route path="/ren" element={<Render />} /> */}
            {/* <Route path="/re" element={<RenderJSON />} /> */}
          </Routes>
        </BrowserRouter>
      </div>
      <Footer />
    </>
  );
};

export default App;
