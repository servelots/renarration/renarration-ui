import React, { useCallback, useEffect, useRef, useState } from "react";
import type { InputRef } from "antd";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const TextEditorComponent = ({
  maxLength,
  placeholder,
  value,
  onChange,
  required,
  editValue,
  updateTextFeild,
  readonly,
}: {
  maxLength: number;
  placeholder?: string;
  value?: string;
  onChange: Function;
  required?: boolean;
  editValue?: string;
  updateTextFeild?;
  readonly?: boolean;
}) => {
  const inputRef = useRef<InputRef>(null);
  const maxWords = maxLength;
  const [{ content, wordCount }, setContent] = useState({
    content: value,
    wordCount: 0,
  });

  useEffect(() => {
    onChange(content);
  }, [content]);

  useEffect(() => {
    if (editValue) {
      setFormattedContent(editValue.toString());
    }
  }, [editValue]);

  const setFormattedContent = useCallback(
    (text) => {
      let words = text.split(" ").filter(Boolean);
      if (words.length > maxWords) {
        setContent({
          content: words.slice(0, maxWords).join(" "),
          wordCount: maxWords,
        });
      } else {
        setContent({ content: text, wordCount: words.length });
      }
    },
    [maxWords, setContent]
  );

  const sharedProps = {
    defaultValue: content,
    ref: inputRef,
    showCount: {
      formatter: (info: {
        value: string;
        count: number;
        maxLength?: number;
        wordCount?: number;
        maxWords?: number;
      }) => {
        return (
          <span>
            {wordCount} / {maxWords} words
          </span>
        );
      },
    },
    maxLength: maxLength,
    style: { width: "100%" },
    allowClear: true,
    autoSize: { minRows: 3, maxRows: 6 },
    placeholder: placeholder,
    required: required,
    readonly,
  };

  return (
    <div>
      <ReactQuill
        theme="snow"
        modules={{
          toolbar: [
            [{ header: "1" }, { header: "2" }, { font: [] }],
            [{ size: [] }],
            ["bold", "italic", "underline", "strike", "blockquote"],
            [
              { list: "ordered" },
              { list: "bullet" },
              { indent: "-1" },
              { indent: "+1" },
            ],
            ["link", "image", "video"],
            ["clean"],
          ],
          clipboard: {
            // toggle to add extra line breaks when pasting HTML:
            matchVisual: true,
          },
        }}
        formats={[
          "header",
          "font",
          "size",
          "bold",
          "italic",
          "underline",
          "strike",
          "blockquote",
          "list",
          "bullet",
          "indent",
          "link",
          "image",
          "video",
        ]}
        bounds={".app"}
        placeholder={"Please add your content here....."}
        value={content}
        onChange={(e) => setFormattedContent(e)}
      />

      <span className="font-normal text-xs text-blueGray-400 p-2 flex items-end justify-end">
        {wordCount}/{maxWords}
      </span>
    </div>
  );
};

TextEditorComponent.defaultProps = {
  maxLength: 250,
  placeholder: "",
  required: false,
  value: "",
};

export default TextEditorComponent;
